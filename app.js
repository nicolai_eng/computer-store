// Get elements
const laptopSelect = document.getElementById("laptops")
const pcPrice =document.getElementById("pcPrice")
const laptopDescription = document.getElementById("laptopDescription")
const laptopImage = document.getElementById("laptopImage")
const buttonLoan = document.getElementById("buttonLoan")
const buttonBank = document.getElementById("buttonBank")
const buttonWork = document.getElementById("buttonWork")
const buttonPayBack = document.getElementById("buttonPayBack")
const loanBalance = document.getElementById("loanBalance")
const loan = document.getElementById("loan")
const laptopTitle = document.getElementById("laptopTitle")
const buyLaptop = document.getElementById("buyLaptop")
const features = document.getElementById("features") 
const moneyBalance = document.getElementById("moneyBalance");
const moneyPay = document.getElementById("moneyPay")

const laptopsURL = "https://noroff-komputer-store-api.herokuapp.com" // Laptop info URL

// Initialize variables
let moneyBalanceVal = 0;
let payVal = 0;
let loanBalanceVal = 0;
let loanPaidBack = true;
let selectedLaptop;
let laptops = [];
let laptopsReal = [];

// Fetch computer info from URL
fetch(laptopsURL + "/computers")
    .then(response => response.json())
    .then(data => laptops = data) // Save laptop data in global variable
    .then(laptops => addLaptopsToSelect(laptops))

// Unpacks elements for each laptop.
const addLaptopsToSelect = (laptops) => {
    laptops.forEach(el => {
        if(!(el.title == undefined)){ // Discards entry if it doesn't have a title.
            addLaptopToSelect(el)
            laptopsReal.push(el) // Creates new list containing the laptops
        }
    })
    
    selectedLaptop = laptopsReal[0] // Chooses first laptop in list as initial
    updateLaptopInfo() // Initializes laptop info on webpage
}

// Updates laptop info on webpage
const updateLaptopInfo = () =>{
    pcPrice.innerText = `${selectedLaptop.price} Kr`
    laptopDescription.innerText = selectedLaptop.description
    laptopImage.src = laptopsURL + "/" + selectedLaptop.image
    laptopTitle.innerText = selectedLaptop.title
    features.innerText = selectedLaptop.specs.join("\n")
}

// Calls updateLaptopInfo() when new is selected.  
const laptopSelected = e => {
    selectedLaptop = laptops[e.target.selectedIndex] // finds new selected laptop
    updateLaptopInfo()
}

// Creates options for Select-laptops
const addLaptopToSelect = (laptop) => {
    const laptopElement = document.createElement("option") // Creates option elements
    laptopElement.value = laptop.id
    laptopElement.appendChild(document.createTextNode(laptop.title))
    laptopSelect.appendChild(laptopElement) // Appends options to Select-laptops
}

// Update money values on webpage 
const updateValues = () => {
    moneyBalance.innerText = `${moneyBalanceVal} Kr`
    moneyPay.innerText = `${payVal} Kr`
    loanBalance.innerText = `${loanBalanceVal} Kr`
}

// Creates loan
const promptLoan = () => {
    if (loanPaidBack){ // checks if last loan has been paid back
        const promptVal = parseFloat(prompt("How many money do you wanna loan?")) // prompts user to put in loan amount
        
        if (!(promptVal > 0) || promptVal > 2*moneyBalanceVal){ // checks if loan amount is valid
            alert("Loan must be: \n 1. Larger than 0 Kr. \n 2. Smaller than 2x your current balance.")
        }
        else { // updates balance values
            moneyBalanceVal += promptVal
            loanBalanceVal += promptVal
            loanPaidBack = false // must pay loan back to make a new one
            showLoan() // Makes loan elements visible
        }
    }
    else{
        alert("You have to pay the last loan back \n before you make a new one!")
    }
    updateValues() // Updates all balances on webpage
}

// Makes loan elements visible:
const showLoan = () => {
    buttonPayBack.style.display = "inline-block"
    loan.style.display = "block"
    loanBalance.style.display = "block"
}

// Hides loan elements:
const HideLoan = () => {
    buttonPayBack.style.display = "none"
    loan.style.display = "none"
    loanBalance.style.display = "none"
}

// Sends Pay to bank
const bankClick = () => {
    if (loanBalanceVal > 0 && !(loanBalanceVal < 0.1*payVal)){ // checks for loan and if 10% of pay is larger than loan.
        moneyBalanceVal += 0.9*payVal // Loan deduction is introduced
        loanBalanceVal -= 0.1*payVal
    }
    else if (loanBalanceVal > 0 && loanBalanceVal < 0.1*payVal){ // 10% of pay is larger than loan --> Entire loan is paid back.
        moneyBalanceVal += payVal - loanPaidVal 
        loanBalanceVal = 0 
        HideLoan() // Hide loan elements
        loanPaidBack = true // New loan can be made
    }
    else { // No loan --> Entire pay is added to bank balance.
        moneyBalanceVal += payVal 
    }
    // Updates balances
    payVal = 0
    updateValues()
}

// Adds 100 Kr when work button is pressed.
const workClick = () => {
    payVal += 100;
    updateValues()
}

// Transfers money from Pay to pay back loan
const payBackLoan = () => {
    if (!(payVal < loanBalanceVal)){ // Checks if Pay is enough to pay back loan:
        moneyBalanceVal += payVal -loanBalanceVal
        loanBalanceVal = 0
        HideLoan()
        loanPaidBack = true // New loan can be made
    }
    else { // Loan is larger than Pay:
        loanBalanceVal -= payVal 
    }
    // Updates balances
    payVal = 0
    updateValues()
}

// Buys laptop if it can be afforded.
const tryBuyLaptop = () => {
    if (!(selectedLaptop.price > moneyBalanceVal)){ // Checks the laptop can be afforded
        moneyBalanceVal -= selectedLaptop.price
        alert(`Congratulation with your new ${selectedLaptop.title}`)
        updateValues()
    }
    else { // The laptop can't be afforded
        alert("You don't have enough money, mate!")
    }
}

// Event listeners
laptopSelect.addEventListener("change", laptopSelected)
buttonLoan.addEventListener("click", promptLoan)
buttonBank.addEventListener("click", bankClick)
buttonWork.addEventListener("click", workClick)
buttonPayBack.addEventListener("click", payBackLoan)
buyLaptop.addEventListener("click", tryBuyLaptop)